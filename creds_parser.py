import pandas as pd

class CredsParser:
    def __init__(self):
        pass

    def parse(self, path):
        self.path = path
        self.df = pd.read_csv(self.path)

        self.creds = {}
        for row_idx, row in self.df.iterrows():
            self.creds[row['Field']] = row['Value']

    def get_creds(self):
        return self.creds

    def print_creds(self):
        print(self.get_creds())

    def refresh(self):
        self.parse(self.path)

if __name__ == '__main__':
    creds_path = 'creds.csv'

    parser = CredsParser()
    parser.parse(creds_path)
    parser.print_creds()