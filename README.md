# PWG Picktime Scheduler

This command-line bot visits [Picktime](https://www.picktime.com/yalepwg) to sign up for sessions (time slots) for any activity, according to your time preferences. The bot enforces the rule that you can schedule only one workout (of any activity) per day. This bot is especially useful for automatically signing up for very competitive sessions, such as Strength sessions; sometimes, new Strength sessions fill up just hours after they open.

This program uses the Python browser automation library [Splinter](https://splinter.readthedocs.io/en/latest/index.html), which is based on [Selenium](https://www.selenium.dev/) and [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) and supports interacting with JavaScript elements. So far in the development of this program, you must interact with the command line and install dependencies yourself. Therefore, this program might not be user-friendly for people without a software engineering background.

## Dependencies

The dependencies are Geckodriver, Splinter, Pandas, and [Schedule](https://pypi.org/project/schedule/).

`$ brew install geckodriver`

`$ pip3 install -r requirements.txt` 

(Depending how you previously instealled `pip`/`pip3`, your might need to call `pip` instead of `pip3`)

## Usage

### Set up config files

1. Rename `config.csv.example` to `config.csv` and edit it as appropriate. Don't edit column names or the "Key" column.
2. Rename `creds.csv.example` to `creds.csv` and edit it as appropriate. Don't edit column names or the "Field" column.
3. Rename `preferred_times.csv.example` to `preferred_times.csv` and edit it as appropriate. "Pref0" is your first preference, "Pref1" is your second preference, all the way until "Pref5." The bot will automatically sign you up for your highest preference that has an open session. It's ok to leave cells blank. Don't edit columns names or the "Day" column.
4. Rename `completed.csv.example` to `completed.csv` and enter the session times that you've already scheduled. This file tells the bot which days to avoid scheduling new sessions on, and the bot appends a new row to the file every time it schedules a new session. Don't edit the column name ("Time"). Tip: you may enter just the days (e.g., `2nd Mar 2021`) instead of the full datetime (e.g., `2nd Mar 2021, 10:15 AM`); the bot only cares about the days, so this is valid input.

### Run

`$ python3 scheduler.py`

You'll be prompted (in the command line) to select whether you want to see debug messages and when you want the scheduler to start scheduling your sessions. You can schedule for ASAP (run once ASAP) or for any time (run for 4 hrs daily).

By default, the bot sleeps until 6:00am (local time) daily, at which point it wakes up and checks the Picktime website every 2 mins until 10:00am (local time). I chose this timeframe because PWG seems to open new sessions sometime during this time frame every day. Leave the bot running in the background for as long as you want (like days or weeks).

Each time the bot checks Picktime, it spins up a new, visible instance of Firefox that quits once the check is done. For minimal inteference with your daily life, consider having the bot run on your computer while you're asleep. This way, the bot will have scheduled your week of workouts for you before you even wake up.

You can quit the bot anytime by the standard CTRL-C (SIGINT) or by closing the shell window. Quitting the bot anytime yields no side effects. Restart the bot using the normal call (`$ python3 scheduler.py`).
