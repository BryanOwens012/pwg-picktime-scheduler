import datetime
import pandas as pd

from browser_wrapper import BrowserWrapper
from preferred_times_parser import PreferredTimesParser
from creds_parser import CredsParser

class SessionsScheduler:
    def __init__(self):
        self.browser_wrapper = BrowserWrapper()

    def set_debug(self, debug):
        self.debug = debug
        self.browser_wrapper.set_debug(self.debug)

    def set_preferred_times(self, preferred_times):
        self.preferred_times = preferred_times

    def set_creds(self, creds):
        self.creds = creds

    def _dateStr2Datetime(self, picktime_datestr):
        split = picktime_datestr.split(' ')
        datestr = split[0][:-2] + split[1] + split[2]  # E.g., "3 Mar 2021"
        dt = datetime.datetime.strptime(datestr, '%d%b%Y')
        return dt

    def _getDayOfWeek(self, picktime_datestr):
        return self._dateStr2Datetime(picktime_datestr).strftime('%a') # Get the initial 3 letters of the day of week

    def _appendToCSV(self, csv_path, text):
        # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.concat.html
        series = pd.read_csv(csv_path, squeeze = True)
        to_append = pd.Series([text], name = 'Time')
        series.append(to_append, ignore_index = True).to_csv(csv_path, index = False)
        return True

    def _isInCSV(self, csv_path, text):
        series = pd.read_csv(csv_path, squeeze = True)
        return any(datestr.startswith(text) for datestr in series.to_list())

    def _scheduleOne(self):
        self.browser_wrapper.open(self.config_params['picktime_url'])

        if not self.browser_wrapper.bypassWelcomeMessage():
            self.die()

        activities = self.browser_wrapper.getOpenActivities()
        if not activities:
            self.die()

        open_sessions = self.browser_wrapper.getOpenSessions(activities, self.config_params['activity'])
        if not open_sessions:
            self.quit()
            return True
        if self.debug:
            print()

        self.browser_wrapper.deleteSessionListAd()

        open_days = set([open_session.split(', ')[0] for open_session in open_sessions.keys()])
        open_days_sorted = list(sorted(open_days, key = lambda open_day: self._dateStr2Datetime(open_day)))

        cnt_signed_up = 0

        for open_day in open_days_sorted:
            if self._isInCSV(self.config_params['completed_path'], open_day):
                continue
            day_of_week = self._getDayOfWeek(open_day)
            if day_of_week in self.preferred_times:
                for preferred_time_in_day in self.preferred_times[day_of_week]:
                    preferred_str = f'{open_day}, {preferred_time_in_day}'
                    if preferred_str in open_sessions:

                        # For debugging, as it doesn't actually sign a person up
                        # print(
                        #     f'Successfully signed up for {preferred_str} for {self.activity}! Check your email for a confirmation from Picktime.')
                        # self._appendToCSV(self.completed_path, preferred_str)
                        # cnt_signed_up += 1
                        # return False

                        if self.browser_wrapper.scheduleOneSession(open_sessions[preferred_str]):
                            print(f'Successfully signed up for {preferred_str} for {self.config_params["activity"]}! Check your email for confirmation from Picktime.')
                            self._appendToCSV(self.config_params['completed_path'], preferred_str)
                            cnt_signed_up += 1
                            return False
                        else:
                            self.die()

        if cnt_signed_up == 0:
            print(
                f'Completed all signups.\nYou can always change your preferred times in {self.config_params["preferred_times_path"]}, or check or edit your completed sessions in {self.config_params["completed_path"]}.')
            return True

    def scheduleAll(self):
        # Default config params
        self.config_path = 'config.csv'
        self.config_params = { 'activity': 'Fitness Center Strength',
                          'picktime_url': 'https://www.picktime.com/yalepwg',
                          'preferred_times_path': 'preferred_times.csv',
                          'creds_path': 'creds.csv',
                          'completed_path': 'completed.csv'
                          }
        try:
            config_params_df = pd.read_csv(self.config_path)
            for row_idx, row in config_params_df.iterrows():
                self.config_params[row['Key']] = row['Value']
            if self.debug:
                print(f'Config params:\n{self.config_params}\n')
        except:
            print(f'Warning: an error occured while trying to read {self.config_path}. Using default config params.\n')

        # Creds
        creds_parser = CredsParser()
        creds_parser.parse(self.config_params['creds_path'])
        creds = creds_parser.get_creds()
        if self.debug:
            print(f'Using the credentials:\n{creds}')

        # Preferred times
        preferred_times_parser = PreferredTimesParser()
        preferred_times_parser.parse(self.config_params['preferred_times_path'])
        preferred_times = preferred_times_parser.get_preferred_times()
        if self.debug:
            print(f'Using the preferred times:\n{preferred_times}')
            print()

        # SessionsScheduler
        self.set_preferred_times(preferred_times)
        self.set_creds(creds)

        # BrowserWrapper
        self.browser_wrapper.set_preferred_times(self.preferred_times)
        self.browser_wrapper.set_creds(self.creds)

        # Keep scheduling individual sessions until all sessions are scheduled
        while True:
            if self._scheduleOne():
                return True

        # Quit
        self.quit()

    def quit(self):
        self.browser_wrapper.quit()

    def die(self):
        print('An error occured. Try again.\n')
        self.quit()
        exit()

if __name__ == '__main__':
    sessions_scheduler = SessionsScheduler()
    sessions_scheduler.set_debug(True)
    sessions_scheduler.scheduleAll()