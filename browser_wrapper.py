from splinter import Browser # docs: https://splinter.readthedocs.io/en/latest/browser.html
# Also requires: $ brew install geckodriver

class BrowserWrapper:
    def __init__(self):
        self._reset_browser()

    def _reset_browser(self):
        self.browser = Browser()

    def _try_click(self, button, success_message = None, failure_message = None):
        try:
            button.click()
            if self.debug and success_message:
                print(success_message)
            return True
        except:
            if failure_message:
                print(failure_message)
            return False

    def set_debug(self, debug):
        self.debug = debug

    def set_preferred_times(self, preferred_times):
        self.preferred_times = preferred_times

    def set_creds(self, creds):
        self.creds = creds

    def open(self, url):
        self.url = url
        self.browser.visit(self.url)

    def bypassWelcomeMessage(self):
        # https://stackoverflow.com/a/46485831
        welcome_message = self.browser.find_by_css('div[class="modal-body"]')

        if welcome_message:
            button = self.browser.find_by_css('button[class="close"]')
            success_message = 'Successfully bypassed the welcome message'
            failure_message = 'Failed to bypass the welcome message'
            return self._try_click(button, success_message, failure_message)
        else:
            if self.debug:
                print('No welcome message detected')
            return True

    def getOpenActivities(self):
        activities_unparsed = self.browser.find_by_css('ul[class="booking-list class-list"] li')
        activities = {activity.text.split('\n')[0]: activity for activity in activities_unparsed}
        activity_names = list(activities.keys())

        if self.debug:
            print(f'Found {len(activities)} activities: {activity_names}')
        # print(activities)
        return activities

    def getOpenSessions(self, activities, activity_name):
        button = activities[activity_name]
        success_message = f'Successfully fetched the open sessions for {activity_name}'
        failure_message = f'Failed to fetch the open sessions for {activity_name}'
        self._try_click(button, success_message, failure_message)

        sessions_unparsed = self.browser.find_by_css('ul[class="booking-list sessions-list"] li')
        sessions = {session.text.split('\n')[0].split(' (GMT')[0]: session for session in sessions_unparsed}
        open_sessions = {session_name: session for session_name, session in sessions.items() if not session.text.split('\n')[0].endswith('(0/15 Available)')}

        # Sort open sessions


        if len(open_sessions) == 0:
            end = '. No open sessions right now :( Try again later\n'
        else:
            end = '\n'

        if self.debug:
            print(f'Found {len(open_sessions)} open sessions out of {len(sessions)} sessions for {activity_name}', end = end)

        return open_sessions

    def scheduleOneSession(self, session):
        button = session
        success_message = f'Successfully loaded the signup page for {session.text}'
        failure_message = f'Failed to load the signup for {session.text}'
        if not self._try_click(button, success_message, failure_message):
            return False

        # Fill in the form to signup
        textboxes_names = {'first_name': 'firstname', 'last_name': 'lastname',
                     'email_id': 'custemail', 'mobile_number': 'custmobile mobilenumber',
                     'in_residence': 'other_of0'
                     }
        for textbox_name, textbox_classname in textboxes_names.items():
            textbox = self.browser.find_by_css(f'input[class="form-control {textbox_classname}"')[0]
            textbox.fill(self.creds[textbox_name])

        button = self.browser.find_by_css('button[class="btn btn-primary margin-bottom booknow"]')[0]
        return self._try_click(button, None, None)

    def deleteSessionListAd(self):
        ad_buttons = self.browser.find_by_css('i[class="icon-cancel"]')
        for ad_button in ad_buttons:
            ''''
            This function is guaranteed to raise an exception at least once, but it has no effect on the page.
            So, we just ignore that exception. The exception looks something like this:

            Element <i class="icon-cancel"> is not clickable at point (992,733)
            because another element <i class="icon-cancel"> obscures it
            '''

            try:
                ad_button.click()
            except:
                pass

    def reset(self, url = None):
        self._reset_browser()
        if url:
            self.open(url)

    def quit(self):
        self.browser.quit()