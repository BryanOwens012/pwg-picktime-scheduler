import schedule
import time
import datetime

from sessions_scheduler import SessionsScheduler

class Scheduler:
    def __init__(self):
        self.run_cnt = 0
        self.debug = True

    def _run_sessions_scheduler(self):
        print(f'\nRun #{self.run_cnt} at {datetime.datetime.now()} =======================\n')

        sessions_scheduler = SessionsScheduler()
        sessions_scheduler.set_debug(self.debug)
        sessions_scheduler.scheduleAll()
        sessions_scheduler.quit()

        self.run_cnt += 1

    def run_n_times(self):
        for i in range(self.n):
            self._run_sessions_scheduler()
            time.sleep(self.sleep_seconds)

    def run(self, time_of_day = '6:00', n = 120, sleep_seconds = 120):
        self.n = n
        self.sleep_seconds = sleep_seconds

        schedule.every().day.at(time_of_day.zfill(5)).do(self.run_n_times)
        print(f'Sleeping until {time_of_day}')

        while True:
            schedule.run_pending()
            time.sleep(1)

if __name__ == '__main__':
    scheduler = Scheduler()

    while True:
        print('Show debug messages? (y/n)')
        debug = input().strip()

        if debug.lower() in ['y', 'yes', 'yep']:
            scheduler.debug = True
            break
        elif debug.lower() in ['n', 'no', 'nope']:
            scheduler.debug = False
            break
        else:
            print('Invalid input. Try again.')

    while True:
        print('When do you want the scheduler to start? Answer either "asap" (run ASAP, and only once), "default" (run at 6:00am daily for 4 hrs), or a 24-hr time — e.g. "13:30" (run at 1:30pm daily for 4 hrs.')
        when = input().strip()

        if when == 'default':
            scheduler.run(time_of_day = '6:00', n = 120, sleep_seconds = 120)
            break
        elif when in ['asap', 'now']:
            scheduler.n = 1
            scheduler.sleep_seconds = 1
            scheduler.run_n_times()
            break
        else:
            try:
                scheduler.run(time_of_day = when, n = 120, sleep_seconds = 120)
                break
            except:
                print('Invalid input. Try again.')
