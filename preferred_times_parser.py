import pandas as pd

class PreferredTimesParser:
    def __init__(self):
        pass

    def parse(self, path):
        self.path = path
        self.df = pd.read_csv(self.path)

        day_abbrevs = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        self.preferred_times = {day_abbrev: list() for day_abbrev in day_abbrevs}

        for row_idx, row in self.df.iterrows():
            # zfill(4) because we want e.g. '08:00 AM'
            self.preferred_times[row['Day']] = [pref.zfill(8) for pref in row.dropna().tolist()[1:]]

    def get_preferred_times(self):
        return self.preferred_times

    def print_preferred_times(self):
        print(self.get_preferred_times())

    def refresh(self):
        self.parse(self.path)

if __name__ == '__main__':
    preferred_times_path = 'preferred_times.csv'

    parser = PreferredTimesParser()
    parser.parse(preferred_times_path)
    parser.print_preferred_times()